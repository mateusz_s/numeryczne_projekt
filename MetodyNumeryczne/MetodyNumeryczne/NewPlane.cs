﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MetodyNumeryczne
{
    public partial class NewPlane : Form
    {
        public double S, AR, e, m, g, rho, CDo, H, R;

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        public NewPlane()
        {
            InitializeComponent();
            DialogResult = DialogResult.Cancel;
        }

        private void Add_Click(object sender, EventArgs e)
        {
            try
            {
                S = (string.IsNullOrWhiteSpace(textBox1.Text)) ? 0.017 : Double.Parse(textBox1.Text);
                AR = (string.IsNullOrWhiteSpace(textBox2.Text)) ? 0.86 : Double.Parse(textBox2.Text);
                this.e = (string.IsNullOrWhiteSpace(textBox3.Text)) ? 0.9 : Double.Parse(textBox3.Text);
                m = (string.IsNullOrWhiteSpace(textBox4.Text)) ? 0.003 : Double.Parse(textBox4.Text);
                g = (string.IsNullOrWhiteSpace(textBox5.Text)) ? 9.8 : Double.Parse(textBox5.Text);
                rho = (string.IsNullOrWhiteSpace(textBox6.Text)) ? 1.225 : Double.Parse(textBox6.Text);
                CDo = (string.IsNullOrWhiteSpace(textBox7.Text)) ? 0.02 : Double.Parse(textBox7.Text);
                H = (string.IsNullOrWhiteSpace(textBox8.Text)) ? 2 : Double.Parse(textBox8.Text);
                R = (string.IsNullOrWhiteSpace(textBox9.Text)) ? 0 : Double.Parse(textBox9.Text);

                DialogResult = DialogResult.OK;
                Close();
            }
            catch
            {
                MessageBox.Show("Źle wpisane dane");
            }
        }
    }
}
