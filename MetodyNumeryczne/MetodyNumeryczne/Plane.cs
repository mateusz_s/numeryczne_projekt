﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.DataVisualization.Charting;
using DotNumerics.LinearAlgebra;
using DotNumerics.ODE;
using System.Diagnostics;

namespace MetodyNumeryczne
{
    class Plane
    {
        public Series Series;
        double S; // Referance Area, m^2
        double AR; // Wing Aspect Ratio
        double e; // Oswald Efficiency Factor
        double m; // Mass, kg
        double g; // gravitational acceleration, m/s^2
        double rho; // Air density at Sea Level, kg/m^3
        double CLa; // Lift-Coefficient Slope, per rad
        double CDo; //Zero-Lift Drag Coefficient
        double epsilon; // Induced Drag Factor
        double CL; // CL for Maximum Lift/Drag Ratio
        double CD; // Corresponding CD
        double LDmax; // Maximum Lift/Drag Ratio
        public double Gam { get; } // Corresponding Flight Path Angle, rad
        public double V { get; } // Corresponding Velocity, m/s
        double Alpha; // Corresponding Angle of Attack, rad

        OdeImplicitRungeKutta5 implicitRK2 = new OdeImplicitRungeKutta5();
        algorithms algorithmsLibrary = new algorithms();
        double[] solutionTime;
        double[,] solution;
        double[] sol = new double[4];
        int solutionIndex = 0;
        public double time;
        double deltaTime;
        public double H;
        public double R;
        Dictionary<double, double> known = new Dictionary<double, double>();
        Logger.logger log = new Logger.logger("Plane");
        

        public Plane(double S, double AR, double e, double m, double g, double rho, double CDo)
        {
            this.S = S;
            this.AR = AR;
            this.e = e;
            this.m = m;
            this.g = g;
            this.rho = rho;
            CLa = Math.PI * AR / (1 + Math.Sqrt(1 + (AR / 2) * (AR / 2)));
            this.CDo = CDo;
            epsilon = 1 / (Math.PI * e * AR);
            CL = Math.Sqrt(CDo / epsilon);
            CD = CDo + epsilon * CL *CL;
            LDmax = CL / CD;
            Gam = -Math.Atan(1 / LDmax);
            V = Math.Sqrt(2 * m * g / (rho * S * (CL * Math.Cos(Gam) - CD * Math.Sin(Gam))));
            Alpha = CL / CLa;

            Series = new Series()
            {
                BorderWidth = 10,
                IsVisibleInLegend = false,
                IsXValueIndexed = true,
                ChartType = SeriesChartType.Point
                
            };

            time = new double();
            deltaTime = 0.1;

        }
        public void startSimulationForInitialValues(double V, double Gamma, double H, double R, double dTime = 0.1)
        {
            sol[0] = 0.1;
            sol[1] = 0;
            sol[2] = H;
            sol[3] = R;
            deltaTime = dTime;
            calculateGlide(time, 100, sol, 10);
            int liczba = known.Count;
            if (liczba < 1000000)
            {
                var scaler = new algorithms.LinearInterpolator(known);
                var start = known.First().Key;
                var end = known.Last().Key;
                double step = 0.0001;
                Series.Points.AddXY(0, H);
                for (var x = start + step; x <= end; x += step)
                {
                    var y = scaler.GetValue(x);
                    Series.Points.AddXY(x, y);
                }
            }
            else
            {
                foreach (var entry in known)
                {
                    
                    Series.Points.AddXY(entry.Key, entry.Value);
                }
            }


            /*
            List<List<TimeSpan>> PomiarCzasu = new List<List<TimeSpan>>();
            List<List<List<Double[]>>> dane = new List<List<List<Double[]>>>();
            double[] step = { 0.0001, 0.1, 1, 2, 5, 10 };
            for(int pomiar = 0; pomiar <6; pomiar++)
            {
                PomiarCzasu.Add(new List<TimeSpan>());
                dane.Add(new List<List<double[]>>());
                Stopwatch timer = new Stopwatch();

                timer.Restart();
                known = new Dictionary<double, double>();

                calculateGlide(time, 100, sol, step[pomiar]);

                PomiarCzasu[pomiar].Add(timer.Elapsed);

                int liczba = known.Count;
                dane[pomiar].Add(new List<double[]>());


                timer.Restart();
                //spline
                if (liczba < 100 / step[0] + 1)
                {
                    var scaler = new algorithms.SplineInterpolator(known);
                    var st = known.First().Key;
                    var en = known.Last().Key;
                    double ste = 0.0001;
                    dane[pomiar][0].Add(new double[2] { 0, H });
                    for (int x = (int)st + 1; x < en / ste; x++)
                    {
                        double x1 = x * ste;
                        var y = scaler.GetValue(x1);
                        dane[pomiar][0].Add(new double[2] { x1, y });
                    }
                }
                else
                {
                    foreach (var entry in known)
                    {
                        dane[pomiar][0].Add(new double[2] {entry.Key, entry.Value });
                    }
                }

                
                PomiarCzasu[pomiar].Add(timer.Elapsed);
                dane[pomiar].Add(new List<double[]>());

                timer.Restart();
                //liniowy
                if (liczba < 100 / step[0] + 1)
                {
                    var scaler = new algorithms.LinearInterpolator(known);
                    var st = known.First().Key;
                    var en = known.Last().Key;
                    double ste = 0.0001;
                    dane[pomiar][1].Add(new double[2] { 0, H });
                    for (int x = (int)st + 1; x < en / ste; x++)
                    {
                        double x1 = x * ste;
                        var y = scaler.GetValue(x1);
                        dane[pomiar][1].Add(new double[2] { x1, y });
                    }
                }
                else
                {
                    var st = known.First().Key;
                    var en = known.Last().Key;
                    foreach (var entry in known)
                    {
                        dane[pomiar][1].Add(new double[2] { entry.Key, entry.Value });
                    }
                }

                
                PomiarCzasu[pomiar].Add(timer.Elapsed);
                PomiarCzasu[pomiar].Add(PomiarCzasu[pomiar][0] + PomiarCzasu[pomiar][1]);
                PomiarCzasu[pomiar].Add(PomiarCzasu[pomiar][0] + PomiarCzasu[pomiar][2]);
                dane[pomiar].Add(new List<double[]>());
                for (int i = 0; i < dane[pomiar][0].Count; i++)
                {
                    dane[pomiar][2].Add(new double[2] { dane[pomiar][0][i][1] - dane[0][0][i][1], dane[pomiar][1][i][1] - dane[0][0][i][1] });
                }

                dane[pomiar].Add(new List<double[]>());
                double[] srednia = new double[2];

                for (int i = 0; i < dane[pomiar][0].Count; i++)
                {
                    srednia[0] += Math.Abs(dane[pomiar][2][i][0]);
                    srednia[1] += Math.Abs(dane[pomiar][2][i][1]);
                }
                srednia[0] = srednia[0] / dane[pomiar][0].Count;
                srednia[1] = srednia[1] / dane[pomiar][0].Count;
                dane[pomiar][3].Add(srednia);
            }
            */

            /*
            int liczba = known.Count;
            if (liczba < 1000000)
            {
                var scaler = new algorithms.SplineInterpolator(known);
                var start = known.First().Key;
                var end = known.Last().Key;
                double step = 0.0001;
                Series.Points.AddXY(0, H);
                for (var x = start+step; x <= end; x += step)
                {
                    var y = scaler.GetValue(x);
                    Series.Points.AddXY(x, y);
                }
            }
            else
            {
                var start = known.First().Key;
                var end = known.Last().Key;
                double step = 0.0001;
                for (var x = start; x <= end; x += step)
                {
                    double y = known[x];
                    Series.Points.AddXY(x, y);
                }
            }
            */



        }
        public void copySolutionValuesToNextInitValues()
        {
            sol[0] = this.solution[1, 0];
            sol[1] = this.solution[1, 1];
            sol[2] = this.solution[1, 2];
            sol[3] = this.solution[1, 3];
        }
        /// <summary>
        /// Timer tu podaje ticki co określony czas z tego wywołujesz GeneratePoint()
        /// </summary>
        public void fly()
        {            
           // calculateGlide(time, time + 0.2, sol, 0.001);
           // GeneratePoint(time, this.solution);
          //  copySolutionValuesToNextInitValues();
           // time += 0.1;
        }

        private void GeneratePoint(double x, double y)
        {
            //Series.Points.AddXY(x, y); 
            known.Add(x, y);
        }
        public void calculateGlide(double t_start, double t_stop, double[] x, double step)
        {
            solutionIndex = 0;
            OdeFunction fun = new OdeFunction(motionEquation);
            OdeSolution sol = new OdeSolution(OdeSol);
            double max_points = (t_stop - t_start) / step;
            solution = new double[int.Parse(max_points.ToString())+1, 4];
            solutionTime = new double[int.Parse(max_points.ToString())+1];
              implicitRK2.InitializeODEs(fun, 4);
              implicitRK2.Solve(x, t_start, step, t_stop, sol);
             
            //algorithms.Equation equ = new algorithms.Equation(x, t_start, step, t_stop);
            //equ.Run(motionEquation, OdeSol, algorithms.RungeKutta.rk4);

            // log.enable();
            //  log.setFilePath("plane.log");
            int index = 0;
            
            for(int i = (int)t_start; i <= t_stop/step; i++)
            {
                GeneratePoint(i*step, solution[index++, 2]);
             //   log.debug(plotTime.ToString() + ":" + val.ToString());
            }
                

        }

        public double[] motionEquation(double t, double[] x)
        {
            double[] output = new double[4];
            double V = x[0];
            double Gam = x[1];
            double q = 0.5 * rho * V * V;
        
            output[0] = (-CD * q * S + m * g * algorithmsLibrary.derivativeOh2(Math.Cos, Gam, 0.1)) / m;
            output[1] = (CL * q * S - m * g * Math.Cos(Gam)) / (m * V);
            output[2] = -V * algorithmsLibrary.derivativeOh2(Math.Cos, Gam, 0.1);
            output[3] = V * Math.Cos(Gam);
            return output;
        }

        private void OdeSol(double t, double[] y)
        {
            solution[solutionIndex, 0] = y[0];
            solution[solutionIndex, 1] = y[1];
            solution[solutionIndex, 2] = y[2];
            solution[solutionIndex, 3] = y[3];
            solutionTime[solutionIndex] = t;
            solutionIndex++;
        }

    }
}
