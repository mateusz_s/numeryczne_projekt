﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace MetodyNumeryczne
{
    public partial class MainForm : Form
    {
        List<Plane> Planes = new List<Plane>();
        TimeSpan count;
        int ticks;
        public MainForm()
        {
            this.PanelP = new Panel();
            this.PanelL1 = new Panel();
            this.PanelL2 = new Panel();
            InitializeComponent();
            
            this.Controls.Add(this.PanelP);
            this.Controls.Add(this.PanelL1);
            this.Controls.Add(this.PanelL2);

            this.Chart = new Chart();

            this.Chart.Name = "Chart";
            this.Chart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Chart.Text = "chart";
            this.Chart.Location = new System.Drawing.Point(0, 0);
            this.PanelP.Controls.Add(this.Chart);

            
            this.Chart.ChartAreas.Add(new System.Windows.Forms.DataVisualization.Charting.ChartArea());
            this.Chart.ChartAreas[0].Name = "chartarea";
            this.Chart.ChartAreas[0].AxisX.Title = "Time(s)";
            this.Chart.ChartAreas[0].AxisX.IsStartedFromZero = true;
            this.Chart.ChartAreas[0].AxisY.Title = "Height(m)";
            this.Chart.ChartAreas[0].AxisX.Minimum = 0;
            this.Chart.ChartAreas[0].AxisY.Minimum = 0;
            

            this.Chart.Legends.Add(new System.Windows.Forms.DataVisualization.Charting.Legend());
            this.Chart.Legends[0].Name = "legend";





           
            

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Height = Screen.PrimaryScreen.WorkingArea.Height / 2;
            this.Width = Screen.PrimaryScreen.WorkingArea.Width / 2;

            //kolory do testów skalowania
            this.PanelP.BackColor = Color.AliceBlue;
            this.PanelL1.BackColor = Color.Azure;
            this.PanelL2.BackColor = Color.Crimson;
            Chart.Series.Clear();

        }

        private void MainForm_Resize(object sender, EventArgs e)
        {

            this.PanelP.Height = this.Height;
            this.PanelP.Width = this.Width * 5 / 8;
            this.PanelP.Top = 0;
            this.PanelP.Left = this.Width * 3 / 8;

            this.PanelL1.Height = this.Height * 6 / 8;
            this.PanelL1.Width = this.Width * 3 / 8;
            this.PanelL1.Top = 0;
            this.PanelL1.Left = 0;

            this.PanelL2.Height = this.Height * 2 / 8;
            this.PanelL2.Width = this.Width * 3 / 8;
            this.PanelL2.Top = this.Height * 6 / 8;
            this.PanelL2.Left = 0;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            //foreach(Plane P in Planes)
            //{
            //   P.fly();
            // }
            ticks++;
        }

        private void newPlaneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            
            NewPlane nowy = new NewPlane();
          
            DialogResult result = nowy.ShowDialog();
            if (result == DialogResult.OK)
            {
                bool TimerRunning = Timer.Enabled;
                Timer.Stop();
                Plane Plane = new Plane(nowy.S, nowy.AR, nowy.e, nowy.m, nowy.g, nowy.rho, nowy.CDo);
                Plane.H = nowy.H;
                Plane.R = nowy.R;
                resetToolStripMenuItem.PerformClick();
                this.Chart.Series.Add(Plane.Series);
                
                Random randomGen = new Random();
                KnownColor[] names = (KnownColor[])Enum.GetValues(typeof(KnownColor));
                KnownColor randomColorName = names[randomGen.Next(names.Length)];
                Plane.Series.Color = Color.FromKnownColor(randomColorName);

                Plane.Series.Name = "Plane " + Planes.Count.ToString();
                Planes.Add(Plane);
                if(TimerRunning)
                    Timer.Start();
            }
            
                
        }

        private void startToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DateTime start = DateTime.Now;
            Timer.Start();
            foreach (Plane P in Planes)
            {
                P.startSimulationForInitialValues(1.5, 0.1, P.H, P.R);
            }
            DateTime stop = DateTime.Now;
            long czas = (stop.Ticks - start.Ticks);
            count = new TimeSpan(czas);
            MessageBox.Show(count.TotalSeconds.ToString());
        }

        private void stopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Timer.Stop();
        }

        private void resetToolStripMenuItem_Click(object sender, EventArgs e)
        {

            foreach(Plane P in Planes)
            {
                P.Series.Points.Clear();
                P.time = 0;
            }
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
