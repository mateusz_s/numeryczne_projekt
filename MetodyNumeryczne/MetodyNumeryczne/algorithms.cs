﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetodyNumeryczne
{
    public class algorithms
    {
        /// <summary>
        /// Przybliżenie wartości pochodnej ilorazem różnicowym z dokładnością O(h)
        /// </summary>
        /// <param name="func"></param>
        /// <param name="x"></param>
        /// <param name="h"></param>
        /// <returns></returns>
        public double derivativeOh(Func<double, double> func, double x, double h)
        {
            return (func(x + h) - func(x)) / h;
        }
        /// <summary>
        /// Przybliżenie wartości pochodnej ilorazem różnicowym z dokładnością O(h^2)
        /// </summary>
        /// <param name="func"></param>
        /// <param name="x"></param>
        /// <param name="h"></param>
        /// <returns></returns>
        public double derivativeOh2(Func<double, double> func, double x, double h)
        {
            return (func(x + h) - func(x-h)) / (2*h);
        }


        public static class RungeKutta
        {
            static double sixth = 1.0 / 6.0;


            public static double[] rk4(double t, double[] x, double dx, Equation.functionDelegate f)
            {
                double halfdx = 0.5 * dx;
                double[] mod_x = x;
                double[] a = f(t, x);
                double[] x_n1 = new double[x.Length];
                /* calculate a */
                for (int i = 0; i < a.Length; ++i)
                {
                    a[i] = a[i] * dx;

                }
                
                /* calculate b */
                for (int i = 0; i < mod_x.Length; ++i)
                {
                    mod_x[i] = x[i] + halfdx*a[i];
                }
                double[] b = f(t+halfdx, mod_x);
                for (int i = 0; i < b.Length; ++i)
                {
                    b[i] = b[i] * dx;

                }

                /* calculate c */
                for (int i = 0; i < mod_x.Length; ++i)
                {
                    mod_x[i] = x[i] + halfdx * b[i];
                }
                double[] c = f(t+halfdx, mod_x);
                for (int i = 0; i < c.Length; ++i)
                {
                    c[i] = c[i] * dx;
                }
                /* calculate d */

                for (int i = 0; i < mod_x.Length; ++i)
                {
                    mod_x[i] = x[i] + dx * c[i];
                }
                double[] d = f(t + dx, mod_x);
                for (int i = 0; i < d.Length; ++i)
                {
                    d[i] = d[i] * dx;
                }

                
                for(int i = 0; i < x.Length; i++)
                {
                    x_n1[i] = x[i] + sixth * (a[i] + 2 * b[i] + 2 * c[i] + d[i]);
                }

                return x_n1;

            }

            public static double[] rk2(double t, double[] x, double dx, Equation.functionDelegate f)
            {
                double halfdx = 0.5 * dx;
                double[] mod_x = x;
                double[] a = f(t, x);
                double[] x_n1 = new double[x.Length];
                /* calculate a */
                for (int i = 0; i < a.Length; ++i)
                {
                    a[i] = a[i] * dx;

                }

                /* calculate b */
                for (int i = 0; i < mod_x.Length; ++i)
                {
                    mod_x[i] = x[i] + halfdx * a[i];
                }
                double[] b = f(t + halfdx, mod_x);
                for (int i = 0; i < b.Length; ++i)
                {
                    b[i] = b[i] * dx;

                }

             
                for (int i = 0; i < x.Length; i++)
                {
                    x_n1[i] = x[i] + dx * (0.5*a[i] + 0.5 * b[i]);
                }

                return x_n1;

            }
        }

        public class Equation
        {
            double[] x;
            double t, dx, target;
            public delegate void solutionDelegate(double t, double[] x);
            public delegate double[] functionDelegate(double t, double[] x);
            public delegate double[] solverDelegate(double t, double[] x, double dx, Equation.functionDelegate f);

            public Equation(double[] x, double t, double dx, double target)
            {
                this.x = x;
                this.t = t;
                this.dx = dx;
                this.target = target;
            }

            public void Run(functionDelegate f, solutionDelegate sol, solverDelegate solver)
            {
                while (t < target)
                {
                    x = solver(t, x, dx, f);
                    sol(t, x);
                    t += dx;
                }
            }
            
        }

        public class LinearInterpolator
        {
            private readonly double[] _keys;

            private readonly double[] _values;

            private readonly double[] _h;

            private readonly double[] _a;

            public LinearInterpolator(IDictionary<double, double> nodes)
            {
                if (nodes == null)
                {
                    throw new ArgumentNullException("nodes");
                }

                var n = nodes.Count;

                if (n < 2)
                {
                    throw new ArgumentException("At least two point required for interpolation.");
                }

                _keys = nodes.Keys.ToArray();
                _values = nodes.Values.ToArray();
            }

            public double GetValue(double key)
            {
                int gap = 1;
                var previous = double.MinValue;
                
                for (int i = 0; i < _keys.Length; i++)
                {
                    if (_keys[i] < key && _keys[i] > previous)
                    {
                        previous = _keys[i];
                        gap = i + 1;
                    }
                }

                return _values[gap - 1] + (_values[gap] - _values[gap - 1]) * ((key - _keys[gap - 1]) / (_keys[gap] - _keys[gap - 1]));
            }
        }
        public class SplineInterpolator
        {
            private readonly double[] _keys;

            private readonly double[] _values;

            private readonly double[] _h;

            private readonly double[] _a;

            /// <summary>
            /// Class constructor.
            /// </summary>
            /// <param name="nodes">Collection of known points for further interpolation.
            /// Should contain at least two items.</param>
            public SplineInterpolator(IDictionary<double, double> nodes)
            {
                if (nodes == null)
                {
                    throw new ArgumentNullException("nodes");
                }

                var n = nodes.Count;

                if (n < 2)
                {
                    throw new ArgumentException("At least two point required for interpolation.");
                }

                _keys = nodes.Keys.ToArray();
                _values = nodes.Values.ToArray();
                _a = new double[n];
                _h = new double[n];

                for (int i = 1; i < n; i++)
                {
                    _h[i] = _keys[i] - _keys[i - 1];
                }

                if (n > 2)
                {
                    var sub = new double[n - 1];
                    var diag = new double[n - 1];
                    var sup = new double[n - 1];

                    for (int i = 1; i <= n - 2; i++)
                    {
                        diag[i] = (_h[i] + _h[i + 1]) / 3;
                        sup[i] = _h[i + 1] / 6;
                        sub[i] = _h[i] / 6;
                        _a[i] = (_values[i + 1] - _values[i]) / _h[i + 1] - (_values[i] - _values[i - 1]) / _h[i];
                    }

                    SolveTridiag(sub, diag, sup, ref _a, n - 2);
                }
            }

            /// <summary>
            /// Gets interpolated value for specified argument.
            /// </summary>
            /// <param name="key">Argument value for interpolation. Must be within 
            /// the interval bounded by lowest ang highest <see cref="_keys"/> values.</param>
            public double GetValue(double key)
            {
                int gap = 1;
                var previous = double.MinValue;

                // At the end of this iteration, "gap" will contain the index of the interval
                // between two known values, which contains the unknown z, and "previous" will
                // contain the biggest z value among the known samples, left of the unknown z
                for (int i = 0; i < _keys.Length; i++)
                {
                    if (_keys[i] < key && _keys[i] > previous)
                    {
                        previous = _keys[i];
                        gap = i + 1;
                    }
                }

                var x1 = key - previous;
                var x2 = _h[gap] - x1;

                return ((-_a[gap - 1] / 6 * (x2 + _h[gap]) * x1 + _values[gap - 1]) * x2 +
                    (-_a[gap] / 6 * (x1 + _h[gap]) * x2 + _values[gap]) * x1) / _h[gap];
            }


            /// <summary>
            /// Solve linear system with tridiagonal n*n matrix "a"
            /// using Gaussian elimination without pivoting.
            /// </summary>
            private static void SolveTridiag(double[] sub, double[] diag, double[] sup, ref double[] b, int n)
            {
                int i;

                for (i = 2; i <= n; i++)
                {
                    sub[i] = sub[i] / diag[i - 1];
                    diag[i] = diag[i] - sub[i] * sup[i - 1];
                    b[i] = b[i] - sub[i] * b[i - 1];
                }

                b[n] = b[n] / diag[n];

                for (i = n - 1; i >= 1; i--)
                {
                    b[i] = (b[i] - sup[i] * b[i + 1]) / diag[i];
                }
            }
        }

    }
    

}
