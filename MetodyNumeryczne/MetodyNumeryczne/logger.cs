﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logger
{
    enum loggerStatus
    {
        disabled,
        enabled,
        undefinedPath,
        writeSuccess,
        error
    }
    class logger
    {
        private bool loggerOn = false;
        private string filePath = "default.log";
        private string component = "";
        public logger(string componentName)
        {
            component = componentName;
            loggerOn = true;
        }
        public void setFilePath(string pathToFile)
        {
            filePath = pathToFile;
        }
        public void enable()
        {
            loggerOn = true;
        }
        public void disable()
        {
            loggerOn = false;
        }
        private loggerStatus print(string text, string level)
        {
            if (!loggerOn) return loggerStatus.disabled; 
            System.IO.StreamWriter logStream = new System.IO.StreamWriter(filePath, true);
            string textToWrite = generateLogLine(text, level);
            logStream.WriteLine(textToWrite);
            logStream.Close();
            return loggerStatus.writeSuccess;  
        }
        public loggerStatus info(string text)
        {
            return print(text, "INF");
        }
        public loggerStatus error(string text)
        {
            return print(text, "ERR");
        }
        public loggerStatus debug(string text)
        {
            return print(text, "DBG");
        }
        private string generateLogLine(string text, string level)
        {
            System.DateTime time = System.DateTime.Now;
            return time.ToString("HH:mm:ss.ffffff") + " " + level + ":" + component + " " + text;
        }
    }
}
