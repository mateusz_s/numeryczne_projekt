﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Logger;
namespace MetodyNumeryczne
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        /*
        a) Equilibrium Glide at Maximum Lift/Drag Ratio
            H		=	2;			% Initial Height, m
            R		=	0;			% Initial Range, m
            to		=	0;			% Initial Time, sec
            tf		=	6;			% Final Time, sec
            tspan	=	[to tf];
            xo		=	[V;Gam;H;R];
            [ta,xa]	=	ode23('EqMotion',tspan,xo);
        Alpha	=	CL / CLa;			% Corresponding Angle of Attack, rad

        %	a) Equilibrium Glide at Maximum Lift/Drag Ratio
        H		=	2;			% Initial Height, m
        R		=	0;			% Initial Range, m
        to		=	0;			% Initial Time, sec
        tf		=	6;			% Final Time, sec
        */
        [STAThread]
        static void Main()
        {
            Plane test = new Plane(0.017, 0.86, 0.9, 0.003, 9.8, 1.225, 0.02);
            double h = 2;
            double r = 0;
            double to = 0;
            double tf = 6;
            double[] x0 = new double[4];
            x0[0] = test.V;
            x0[1] = test.Gam;
            x0[2] = h;
            x0[3] = r;
       //     test.calculateGlide(to, tf, x0, 1);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
